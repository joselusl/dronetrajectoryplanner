//////////////////////////////////////////////////////
//  spaceMap.cpp
//
//  Created on: Jul 3, 2013
//      Author: joselusl
//
//  Last modification on: Oct 26, 2013
//      Author: joselusl
//
//////////////////////////////////////////////////////


#include "spaceMap.h"





////////////////////// ObstacleInMap /////////////////////

ObstacleInMap::ObstacleInMap()
{
    init();
    return;
}


ObstacleInMap::~ObstacleInMap()
{
    clear();
    return;
}


int ObstacleInMap::init()
{
    obstacleId=-1;
    obstacleType=ObstacleTypes::undefined;
    obstacle_nature_=ObstacleNature::static_obstacle;

    return 1;
}


int ObstacleInMap::clear()
{
    variancesObstacle.clear();

    return 1;
}


int ObstacleInMap::setObstacleId(unsigned int obstacleIdIn)
{
    obstacleId=obstacleIdIn;
    return 1;
}


unsigned int ObstacleInMap::getObstacleId() const
{
    return obstacleId;
}


int ObstacleInMap::setObstacleType(ObstacleTypes obstacleTypeIn)
{
    obstacleType=obstacleTypeIn;
    return 1;
}


ObstacleTypes ObstacleInMap::getObstacleType() const
{
    return obstacleType;
}


double ObstacleInMap::distanceToObstacle(std::vector<double> pointIn, std::vector<double> robotDimensions, int distance_metric) const
{
    double distance=0;
    switch(distance_metric)
    {
        case 1:
            distance=implicitEquation(pointIn, robotDimensions);
            break;
        default:
            distance=0;
            break;
    }
    return distance;
}





////////////////////// EllipseObstacle2d /////////////////////


EllipseObstacle2d::EllipseObstacle2d()
{
    init();
    return;
}


EllipseObstacle2d::~EllipseObstacle2d()
{
    clear();
    return;
}


int EllipseObstacle2d::init()
{
    obstacleType=ObstacleTypes::ellipse_2d;
    centerPoint.resize(2);
    radius.resize(2);
    variancesObstacle.resize(2);

    return 1;
}

int EllipseObstacle2d::clear()
{
    ObstacleInMap::clear();

    centerPoint.clear();
    radius.clear();
    variancesObstacle.clear();

    return 1;
}


int EllipseObstacle2d::getParameters(unsigned int& idOut, std::vector<double>& centerPointOut, std::vector<double>& radiusOut, double& yawAngleOut) const
{
    idOut=obstacleId;
    centerPointOut=centerPoint;
    radiusOut=radius;
    yawAngleOut=yawAngle;
    return 1;
}


int EllipseObstacle2d::setParameters(unsigned int idIn, std::vector<double> centerPointIn, std::vector<double> radiusIn, double yawAngleIn)
{
    return define(idIn,centerPointIn,radiusIn,yawAngleIn);
}


int EllipseObstacle2d::define(unsigned int id, std::vector<double> centerPointIn, std::vector<double> radiusIn, double yawAngleIn)
{
    setObstacleType(ObstacleTypes::ellipse_2d);

    obstacleId=id;
    centerPoint=centerPointIn;
    radius=radiusIn;
    yawAngle=yawAngleIn;

    return 1;
}


double EllipseObstacle2d::implicitEquation(std::vector<double> pointIn, std::vector<double> robotDimensions) const
{
    //cout<<"Ellipse implicit eq"<<endl;

    std::vector<double> pointRotated(2);

    pointRotated[0]=(pointIn[0]-centerPoint[0])*cos(yawAngle)        +(pointIn[1]-centerPoint[1])*sin(yawAngle);
    pointRotated[1]=(pointIn[0]-centerPoint[0])*(-1.0)*sin(yawAngle) +(pointIn[1]-centerPoint[1])*cos(yawAngle);


    double value=0.0;


    value=pow(pointRotated[0]/(radius[0]+robotDimensions[0]/2.0),2)+pow(pointRotated[1]/(radius[1]+robotDimensions[1]/2.0),2)-1;


    return value;
}





////////////////////// RectangleObstacle2d /////////////////////


RectangleObstacle2d::RectangleObstacle2d()
{
    init();
    return;
}


RectangleObstacle2d::~RectangleObstacle2d()
{
    clear();
    return;
}


int RectangleObstacle2d::init()
{
    obstacleType=ObstacleTypes::rectangle_2d;
    centerPoint.resize(2);
    size.resize(2);
    variancesObstacle.resize(2);

    return 1;
}

int RectangleObstacle2d::clear()
{
    ObstacleInMap::clear();

    centerPoint.clear();
    size.clear();
    variancesObstacle.clear();

    return 1;
}


int RectangleObstacle2d::getParameters(unsigned int& idOut, std::vector<double>& centerPointOut, std::vector<double>& sizeOut, double& yawAngleOut) const
{
    idOut=obstacleId;
    centerPointOut=centerPoint;
    sizeOut=size;
    yawAngleOut=yawAngle;
    return 1;
}


int RectangleObstacle2d::setParameters(unsigned int idIn, std::vector<double> centerPointIn, std::vector<double> sizeIn, double yawAngleIn)
{
    return define(idIn,centerPointIn,sizeIn,yawAngleIn);
}


int RectangleObstacle2d::define(unsigned int id, std::vector<double> centerPointIn, std::vector<double> sizeIn, double yawAngleIn)
{
    setObstacleType(ObstacleTypes::rectangle_2d);

    obstacleId=id;
    centerPoint=centerPointIn;
    size=sizeIn;
    yawAngle=yawAngleIn;

    return 1;
}


double RectangleObstacle2d::implicitEquation(std::vector<double> pointIn, std::vector<double> robotDimensions) const
{
    //http://math.stackexchange.com/questions/69099/equation-of-a-rectangle

    //std::cout<<"Rectangle implicit eq"<<std::endl;

    std::vector<double> pointRotated(2);

    pointRotated[0]=(pointIn[0]-centerPoint[0])*cos(yawAngle)        + (pointIn[1]-centerPoint[1])*sin(yawAngle);
    pointRotated[1]=(pointIn[0]-centerPoint[0])*(-1.0)*sin(yawAngle) + (pointIn[1]-centerPoint[1])*cos(yawAngle);


    double value=0.0;


    //value=pow((pointRotated[0]-centerPoint[0])/(radius[0]+robotDimensions[0]),2)+pow((pointRotated[1]-centerPoint[1])/(radius[1]+robotDimensions[1]),2)-1;

    double p=size[0]/2.0+robotDimensions[0]/2.0;
    double q=size[1]/2.0+robotDimensions[1]/2.0;

    value=std::abs(pointRotated[0]/p + pointRotated[1]/q ) + std::abs( pointRotated[0]/p - pointRotated[1]/q ) - 2.0;

    //std::cout<<"Rectangle id="<<obstacleId<<"; impl_eq_value="<<value<<std::endl;

    return value;
}





////////////////////// WorldMap /////////////////////
WorldMap::WorldMap()
{
    init();
    return;
}


WorldMap::~WorldMap()
{
    clear();
    return;
}

int WorldMap::init()
{
    return 1;
}

int WorldMap::clear()
{
    // Delete obstacles_list
    while(!obstaclesList.empty())
    {
        delete obstaclesList.front();
        obstaclesList.pop_front();
    }
    obstaclesList.clear(); // Just in case

    dimension.clear();
    initPoint.clear();

    return 1;
}



int WorldMap::setDimensions(std::vector<double> dimensionIn)
{
    //cout<<"MyMap dim="<<dimensionIn[0]<<";"<<dimensionIn[1]<<endl;
    if(dimensionIn.size()<dimension.size())
        return 0;

    for(unsigned int i=0;i<dimension.size();i++)
        dimension[i]=dimensionIn[i];

    return 1;
}


int WorldMap::getDimensions(std::vector<double> &dimensionOut) const
{
    dimensionOut=dimension;
    return 1;
}


int WorldMap::getDimension(unsigned int &dimensionNum) const
{
    dimensionNum=dimension.size();
    return 1;
}

int WorldMap::setInitPoint(std::vector<double> initPointIn)
{
    initPoint=initPointIn;
    return 1;
}

int WorldMap::getInitPoint(std::vector<double> &initPointOut) const
{
    initPointOut=initPoint;
    return 1;
}


int WorldMap::findObstacle(std::list<ObstacleInMap*>::iterator& it_obstacles_list, unsigned int idObstacle)
{
    for(it_obstacles_list=obstaclesList.begin(); it_obstacles_list!=obstaclesList.end(); ++it_obstacles_list)
    {
        if((*it_obstacles_list)->getObstacleId()==idObstacle)
        {
            return 1;
        }
    }

    //cout<<"error finding obstacles inside WorldMap::findObstacle"<<endl;
    return 0;
}


bool WorldMap::existObstacle(unsigned int idObstacle) const
{
    for(std::list<ObstacleInMap*>::const_iterator it_obstacles_list=obstaclesList.begin(); it_obstacles_list!=obstaclesList.end(); ++it_obstacles_list)
    {
        if((*it_obstacles_list)->getObstacleId()==idObstacle)
        {
            return true;
        }
    }
    return false;
}



int WorldMap::deleteObstacle(unsigned int idObstacle)
{
    std::list<ObstacleInMap*>::iterator it_obstacle_list;

    if(!findObstacle(it_obstacle_list, idObstacle))
    {
        //cout<<"error finding obstacles inside WorldMap::deleteObstacle"<<endl;
        return 0;
    }

    obstaclesList.erase(it_obstacle_list);

    return 1;
}

int WorldMap::listIdObstacles() const
{
#if VERBOSE_SPACE_MAP
    std::cout<<"[WM] obstacles in map: ";
    for(std::list<ObstacleInMap*>::const_iterator it_obstacles_list=obstaclesList.begin(); it_obstacles_list!=obstaclesList.end(); ++it_obstacles_list)
    {
        std::cout<<(*it_obstacles_list)->getObstacleId()<<"; ";
    }
    std::cout<<std::endl;
#endif

    return 1;
}

int WorldMap::listObstacles() const
{
#if VERBOSE_SPACE_MAP
    std::cout<<"[WM] obstacles in map: ";
#endif

    return 1;
}



////////////////////// WorldMap2d /////////////////////

WorldMap2d::WorldMap2d()
{
    init();

    return;
}


WorldMap2d::~WorldMap2d()
{
    clear();
    return;
}

int WorldMap2d::init()
{
    dimension.resize(2);
    return 1;
}

int WorldMap2d::clear()
{

    return 1;
}



int WorldMap2d::addEllipseObstacle2d(unsigned int idObstacle, std::vector<double> centerPointIn, std::vector<double> radiusIn, double yawAngleIn)
{
    if(existObstacle(idObstacle))
        return 0;


    EllipseObstacle2d* OneEllipsePtr=new EllipseObstacle2d;

    if(!OneEllipsePtr->define(idObstacle,centerPointIn,radiusIn,yawAngleIn))
        return 0;


    obstaclesList.push_back(OneEllipsePtr);


    return 1;
}


int WorldMap2d::updateEllipseObstacle2d(unsigned int idObstacle, std::vector<double> centerPointIn, std::vector<double> radiusIn, double yawAngleIn)
{

    std::list<ObstacleInMap*>::iterator it_obstacles_list;
    if(!findObstacle(it_obstacles_list, idObstacle))
        return 0;

    EllipseObstacle2d* OneEllipsePtr=static_cast<EllipseObstacle2d*>(*it_obstacles_list);


    if(!OneEllipsePtr->define(idObstacle,centerPointIn,radiusIn,yawAngleIn))
        return 0;

    return 1;
}


int WorldMap2d::addRectangleObstacle2d(unsigned int idObstacle, std::vector<double> centerPointIn, std::vector<double> sizeIn, double yawAngleIn)
{
    if(existObstacle(idObstacle))
        return 0;

    RectangleObstacle2d* OneEllipsePtr=new RectangleObstacle2d;

    if(!OneEllipsePtr->define(idObstacle,centerPointIn,sizeIn,yawAngleIn))
    {
        return 0;
    }

    obstaclesList.push_back(OneEllipsePtr);

    idObstacle=obstaclesList.size()-1;

    return 1;
}


int WorldMap2d::updateRectangleObstacle2d(unsigned int idObstacle, std::vector<double> centerPointIn, std::vector<double> sizeIn, double yawAngleIn)
{   
    std::list<ObstacleInMap*>::iterator it_obstacles_list;
    if(!findObstacle(it_obstacles_list, idObstacle))
        return 0;

    RectangleObstacle2d* OneEllipsePtr=static_cast<RectangleObstacle2d*>(*it_obstacles_list);

    if(!OneEllipsePtr->define(idObstacle,centerPointIn,sizeIn,yawAngleIn))
        return 0;

    return 1;
}



int WorldMap2d::listObstacles() const
{
#if VERBOSE_SPACE_MAP
    std::cout<<"[WM2d] obstacles in map: "<<std::endl;

    for(std::list<ObstacleInMap*>::const_iterator it_obstacles_list=obstaclesList.begin(); it_obstacles_list!=obstaclesList.end(); ++it_obstacles_list)
    {
        switch((*it_obstacles_list)->getObstacleType())
        {
            case ObstacleTypes::ellipse_2d:
            {
                //Elipse
                EllipseObstacle2d* ThisEllipse=static_cast<EllipseObstacle2d*>(*it_obstacles_list);

                //
                unsigned int idOut;
                std::vector<double> centerPointOut;
                std::vector<double> radiusOut;
                double yawAngleOut;
                ThisEllipse->getParameters(idOut,centerPointOut,radiusOut,yawAngleOut);

                std::cout<<"[WM2d] Ellipse id="<<idOut<<"; centerPoint="<<centerPointOut[0]<<";"<<centerPointOut[1]<<"; radius="<<radiusOut[0]<<";"<<radiusOut[1]<<"; yaw="<<yawAngleOut<<std::endl;

                break;
            }
            case ObstacleTypes::rectangle_2d:
            {
                //Rectangle
                RectangleObstacle2d* ThisEllipse=static_cast<RectangleObstacle2d*>(*it_obstacles_list);

                //
                unsigned int idOut;
                std::vector<double> centerPointOut;
                std::vector<double> radiusOut;
                double yawAngleOut;
                ThisEllipse->getParameters(idOut,centerPointOut,radiusOut,yawAngleOut);

                std::cout<<"[WM2d] Rectangle id="<<idOut<<"; centerPoint="<<centerPointOut[0]<<";"<<centerPointOut[1]<<"; radius="<<radiusOut[0]<<";"<<radiusOut[1]<<"; yaw="<<yawAngleOut<<std::endl;

                break;
            }
            default:
                std::cout<<"[WM2d] Obstacle error"<<std::endl;
                break;

        }
    }


#endif

    return 1;
}

