//////////////////////////////////////////////////////
//  spaceMap.h
//
//  Created on: Jul 3, 2013
//      Author: joselusl
//
//  Last modification on: Oct 26, 2013
//      Author: joselusl
//
//////////////////////////////////////////////////////

#ifndef SPACE_MAP_H
#define SPACE_MAP_H



//C Math
//sin(), cos(), pow(), abs()
#include <cmath>

//I/O Steam
//std::cout
#include <iostream>

//Vector
//std::vector
#include <vector>

// List
#include <list>



#define VERBOSE_SPACE_MAP 1




enum class ObstacleTypes
{
    undefined=0,
    ellipse_2d,
    rectangle_2d
};

enum class ObstacleNature
{
    static_obstacle=0,
    moving_obstacle
};




/////////////////////////////////////////
// Class ObstacleInMap
//
//   Description
//
/////////////////////////////////////////
class ObstacleInMap
{
    //Obstacle Id
protected:
    unsigned int obstacleId;

    //Type of obstacle
protected:
    ObstacleTypes obstacleType;

    // Nature of the obstacle
    // TODO include
protected:
    ObstacleNature obstacle_nature_;

    //Variances definition
public:
    std::vector<double> variancesObstacle;

public:
    ObstacleInMap();
    virtual ~ObstacleInMap();

    int init();
    int clear();

public:
    int setObstacleId(unsigned int obstacleIdIn);
    unsigned int getObstacleId() const;

public:
    int setObstacleType(ObstacleTypes obstacleTypeIn);
    ObstacleTypes getObstacleType() const;

public:
    double distanceToObstacle(std::vector<double> pointIn, std::vector<double> robotDimensions, int distance_metric=1) const;

    // Implicit equation as a metric of the distance to the obstacle
public:
    virtual double implicitEquation(std::vector<double> pointIn, std::vector<double> robotDimensions) const=0;

};



/////////////////////////////////////////
// Class EllipseObstacle2d
//
//   Description
//
/////////////////////////////////////////
class EllipseObstacle2d : public ObstacleInMap
{
    //Object definition
protected:
    std::vector<double> centerPoint;
    std::vector<double> radius;
    double yawAngle;


public:
    EllipseObstacle2d();
    ~EllipseObstacle2d();

    int init();
    int clear();

public:
    int getParameters(unsigned int& idOut, std::vector<double>& centerPointOut, std::vector<double>& radiusOut, double& yawAngleOut) const;
    int setParameters(unsigned int idIn, std::vector<double> centerPointIn, std::vector<double> radiusIn, double yawAngleIn); //equivalent to define

public:

    int define(unsigned int id, std::vector<double> centerPointIn, std::vector<double> radiusIn, double yawAngleIn);

    double implicitEquation(std::vector<double> pointIn, std::vector<double> robotDimensions) const;

};


/////////////////////////////////////////
// Class RectangleObstacle2d
//
//   Description
//
/////////////////////////////////////////
class RectangleObstacle2d : public ObstacleInMap
{
    //Object definition
protected:
    std::vector<double> centerPoint;
    std::vector<double> size;
    double yawAngle;


public:
    RectangleObstacle2d();
    ~RectangleObstacle2d();

    int init();
    int clear();

    //TODO JL
public:
    int getParameters(unsigned int& idOut, std::vector<double>& centerPointOut, std::vector<double>& sizeOut, double& yawAngleOut) const;
    int setParameters(unsigned int idIn, std::vector<double> centerPointIn, std::vector<double> sizeIn, double yawAngleIn); //equivalent to define


public:

    int define(unsigned int id, std::vector<double> centerPointIn, std::vector<double> sizeIn, double yawAngleIn);

    double implicitEquation(std::vector<double> pointIn, std::vector<double> robotDimensions) const;


};


/////////////////////////////////////////
// Class EllipsoidObstacle3d
//
//   Description TODO
//
/////////////////////////////////////////
class EllipsoidObstacle3d : public ObstacleInMap
{

};



/////////////////////////////////////////
// Class PrismObstacle3d
//
//   Description TODO
//
/////////////////////////////////////////
class PrismObstacle3d : public ObstacleInMap
{

};


/////////////////////////////////////////
// Class CylinderObstacle3d
//
//   Description TODO
//
/////////////////////////////////////////
class CylinderObstacle3d : public ObstacleInMap
{

};




/////////////////////////////////////////
// Class WorldMap
//
//   Description
//
/////////////////////////////////////////
class WorldMap
{
    // TODO remove!
    //Size
protected:
    std::vector<double> dimension;
    std::vector<double> initPoint;


public:
    int setDimensions(std::vector<double> dimensionIn);
    int getDimensions(std::vector<double> &dimensionOut) const;

public:
    int getDimension(unsigned int &dimensionNum) const;

public:
    int setInitPoint(std::vector<double> initPointIn);
    int getInitPoint(std::vector<double> &initPointOut) const;


public:
    //Obstacles
    std::list<ObstacleInMap*> obstaclesList;


public:
    WorldMap();
    ~WorldMap();

    int init();
    int clear();

protected:
    int findObstacle(std::list<ObstacleInMap*>::iterator &it_obstacles_list, unsigned int idObstacle);

public:
    bool existObstacle(unsigned int idObstacle) const;

public:
    int deleteObstacle(unsigned int idObstacle);



    //For debugging
public:
    int listIdObstacles() const;
    virtual int listObstacles() const;

};



/////////////////////////////////////////
// Class WorldMap2d
//
//   Description
//
/////////////////////////////////////////
class WorldMap2d : public WorldMap
{
public:
    WorldMap2d();
    ~WorldMap2d();

    int init();
    int clear();

    // Ellipse obstacle 2d
public:
    int addEllipseObstacle2d(unsigned int idObstacle, std::vector<double> centerPointIn, std::vector<double> radiusIn, double yawAngleIn);
    int updateEllipseObstacle2d(unsigned int idObstacle, std::vector<double> centerPointIn, std::vector<double> radiusIn, double yawAngleIn);

    //Rectangle obstacle 2d
public:
    int addRectangleObstacle2d(unsigned int idObstacle, std::vector<double> centerPointIn, std::vector<double> sizeIn, double yawAngleIn);
    int updateRectangleObstacle2d(unsigned int idObstacle, std::vector<double> centerPointIn, std::vector<double> sizeIn, double yawAngleIn);

    //For debugging
public:
    int listObstacles() const;

};


/////////////////////////////////////////
// Class WorldMap3d
//
//   Description TODO
//
/////////////////////////////////////////
class WorldMap3d: public WorldMap
{

};





#endif
