cmake_minimum_required(VERSION 2.8.3)

set(PROJECT_NAME droneTrajectoryPlanner)
project(${PROJECT_NAME})

# check c++11 / c++0x
include(CheckCXXCompilerFlag)
CHECK_CXX_COMPILER_FLAG("-std=c++11" COMPILER_SUPPORTS_CXX11)
CHECK_CXX_COMPILER_FLAG("-std=c++0x" COMPILER_SUPPORTS_CXX0X)
if(COMPILER_SUPPORTS_CXX11)
    #set(CMAKE_CXX_FLAGS "-std=c++11")
    # http://stackoverflow.com/questions/37621342/cmake-will-not-compile-to-c-11-standard
    # CMake below 3.1
    add_definitions(-std=c++11)
    # CMake 3.1 and above
    set(CMAKE_CXX_STANDARD 11) # C++11...
    set(CMAKE_CXX_STANDARD_REQUIRED ON) #...is required...
    set(CMAKE_CXX_EXTENSIONS OFF) #...without compiler extensions like gnu++11
elseif(COMPILER_SUPPORTS_CXX0X)
    #set(CMAKE_CXX_FLAGS "-std=c++0x")
    add_definitions(-std=c++0x)
    message(FATAL_ERROR "The compiler ${CMAKE_CXX_COMPILER} has no C++11 support. Please use a different C++ compiler.")
else()
    message(FATAL_ERROR "The compiler ${CMAKE_CXX_COMPILER} has no C++11 support. Please use a different C++ compiler.")
endif()


# Set the build type.  Options are:
#  Coverage       : w/ debug symbols, w/o optimization, w/ code-coverage
#  Debug          : w/ debug symbols, w/o optimization
#  Release        : w/o debug symbols, w/ optimization
#  RelWithDebInfo : w/ debug symbols, w/ optimization
#  MinSizeRel     : w/o debug symbols, w/ optimization, stripped binaries




set(DRONETRAJECTORYPLANNER_SOURCE_DIR
	src/sources)
	
set(DRONETRAJECTORYPLANNER_INCLUDE_DIR
	src/include
	)

set(DRONETRAJECTORYPLANNER_SOURCE_FILES

	#General
	src/sources/droneTrajectoryPlanner.cpp
	
	
	src/sources/spaceMap.cpp
	src/sources/probabilisticRoadMap.cpp
	src/sources/potencialFieldMap.cpp
	src/sources/discreteSearch.cpp
	src/sources/trajectoryPlanner.cpp
	
	)
	
set(DRONETRAJECTORYPLANNER_HEADER_FILES

	#General
	src/include/droneTrajectoryPlanner.h
	
	src/include/spaceMap.h
	src/include/probabilisticRoadMap.h
	src/include/potencialFieldMap.h
	src/include/discreteSearch.h
	src/include/trajectoryPlanner.h
	
	
	)
	
	
	
#tests
set(DRONETRAJECTORYPLANNER_TEST_SOURCE_FILES

	#General
        src/sources/testDroneTrajectoryPlanner2d_01.cpp
		
	)
	

find_package(catkin REQUIRED
		COMPONENTS pugixml lib_cvgutils)

catkin_package(
        INCLUDE_DIRS ${DRONETRAJECTORYPLANNER_INCLUDE_DIR}
        LIBRARIES ${PROJECT_NAME}
        CATKIN_DEPENDS pugixml lib_cvgutils
  )

include_directories(${DRONETRAJECTORYPLANNER_INCLUDE_DIR})
include_directories(${catkin_INCLUDE_DIRS})




add_library(${PROJECT_NAME} ${DRONETRAJECTORYPLANNER_SOURCE_FILES} ${DRONETRAJECTORYPLANNER_HEADER_FILES})
add_dependencies(${PROJECT_NAME} ${catkin_EXPORTED_TARGETS})
target_link_libraries(${PROJECT_NAME} ${catkin_LIBRARIES})


add_executable(${PROJECT_NAME}_2d_test ${DRONETRAJECTORYPLANNER_TEST_SOURCE_FILES})
add_dependencies(${PROJECT_NAME}_2d_test ${catkin_EXPORTED_TARGETS})
target_link_libraries(${PROJECT_NAME}_2d_test ${PROJECT_NAME})
target_link_libraries(${PROJECT_NAME}_2d_test ${catkin_LIBRARIES})
